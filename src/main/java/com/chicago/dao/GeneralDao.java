package com.chicago.dao;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class GeneralDao extends AbstractDao<Integer, Object>{

    public List getAll(Class c) {
        return getSession().createCriteria(c).list();
    }

    public List getByType(Class c, String type,String field) {
        return getSession().createCriteria(c)
                .add(Restrictions.eq("requestType", type))
                .setProjection(Projections.property(field))
                .list();
    }
}
