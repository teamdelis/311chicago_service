package com.chicago.dao;

import com.chicago.domain.User;
import com.chicago.service.LoggingService;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class UserDao extends AbstractDao<String, User> {

    @Autowired
    LoggingService loggingService;


    public User getById(String id) {
        return getByKey(id);
    }

    public User login(String username, String password) {

        CriteriaQuery<User> criteriaQuery = createEntityCriteria().createQuery(User.class);
        Root<User> u = criteriaQuery.from(User.class);

        criteriaQuery.select(u);
        criteriaQuery.where(createEntityCriteria().equal(u.get("username"),username),
                    createEntityCriteria().equal(u.get("password"), DigestUtils.md5Hex(password)));

        List<User> users = getSession().createQuery(criteriaQuery).getResultList();

        if (users.size() != 0)
            return users.get(0);
        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "login(" +username+ "," + password + ")");
        return null;
    }

    public boolean exists(User user) {
       if(getById(user.getUsername()) == null)
           return false;
       return true;
    }


    public void update(User user) {
        getSession().merge(user);
    }

    public List getAllUsernames() {
        return getSession().createCriteria(User.class)
                .setProjection(Projections.property("username"))
                .list();
    }
}
