package com.chicago.dao;

import com.chicago.domain.Request;
import com.chicago.service.LoggingService;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

@Repository
@Transactional
public class RequestDao extends AbstractDao<Integer, Request>{


    @Autowired
    LoggingService loggingService;

    public Request getById(int id) {
        return getByKey(id);
    }


    public List<Object> query1(String duplicates, String from, String to) throws ParseException {


        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("total_requests_per_type_in_period")
                .registerStoredProcedureParameter(
                        "from",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "to",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("from",parseDate(from))
                .setParameter("to",parseDate(to))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "total_requests_per_type_in_period(" +duplicates + "," + from + "," + to +")" );
        return spQuery.getResultList();
    }

    public List<Object> query2(String type, String duplicates, String from, String to) throws ParseException {


        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("total_requests_per_day_for_type_in_period")
                .registerStoredProcedureParameter(
                        "from",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "to",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "type",
                        String.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("from",parseDate(from))
                .setParameter("to",parseDate(to))
                .setParameter("type",type)
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "total_requests_per_day_for_type_in_period(" + type +  "," +  duplicates + "," + from + "," + to +")");
        return spQuery.getResultList();
    }

    public List<Object> query3(String duplicates, String date) throws ParseException {


        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("most_common_request_type_per_zipcode_in_day")
                .registerStoredProcedureParameter(
                        "date",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("date",parseDate(date))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "most_common_request_type_per_zipcode_in_day(" +duplicates + "," + date + ")");
        return spQuery.getResultList();
    }


    public List<Object> query4(String duplicates, String from, String to) throws ParseException {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("avg_request_completion_time_per_type_in_period")
                .registerStoredProcedureParameter(
                        "from",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "to",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("from",parseDate(from))
                .setParameter("to",parseDate(to))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "avg_request_completion_time_per_type_in_period(" +duplicates + "," + from + "," + to +")");
        return spQuery.getResultList();
    }

    public List<Object> query5(String x1, String y1, String x2, String y2, String duplicates, String date) throws ParseException {


        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("most_common_request_type_within_coords")
                .registerStoredProcedureParameter(
                        "x1",
                        Long.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "y1",
                        Long.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "x2",
                        Long.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "y2",
                        Long.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "date",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery
                .setParameter("x1",Long.parseLong(x1))
                .setParameter("y1",Long.parseLong(y1))
                .setParameter("x2",Long.parseLong(x2))
                .setParameter("y2",Long.parseLong(y2))
                .setParameter("date",parseDate(date))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "most_common_request_type_within_coords(" +x1 + "," + y1 + "," + x2 + "," + y2 + "," + duplicates + "," + date+")");
        return spQuery.getResultList();
    }

    public List<Object> query6(String duplicates, String from, String to) throws ParseException {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("top5_ssa_in_requests_per_day_in_period")
                .registerStoredProcedureParameter(
                        "from",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "to",
                        Date.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("from",parseDate(from))
                .setParameter("to",parseDate(to))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "top5_ssa_in_requests_per_day_in_period(" +duplicates + "," + from + "," + to +")");
        return spQuery.getResultList();
    }

    public List<Object> query7(String duplicates) throws ParseException {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("license_plate_involved_in_multiple_requests")
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "license_plate_involved_in_multiple_requests(" +duplicates + ")");
        return spQuery.getResultList();
    }

    public List<Object> query8(String duplicates) throws ParseException {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("second_most_common_color_of_abandoned_vehicle")
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "second_most_common_color_of_abandoned_vehicle(" +duplicates +")");
        return spQuery.getResultList();
    }

    public List<Object> query9(String numberOfPremises, String duplicates)  {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("requests_with_premises_baited_less_than_specified")
                .registerStoredProcedureParameter(
                        "number_of_premises",
                        Integer.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("number_of_premises",Integer.parseInt(numberOfPremises))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "requests_with_premises_baited_less_than_specified(" +duplicates + "," + numberOfPremises+ ")");
        return spQuery.getResultList();
    }

    public List<Object> query10(String numberOfPremises, String duplicates)  {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("requests_with_premises_with_garbage_less_than_specified")
                .registerStoredProcedureParameter(
                        "number_of_premises",
                        Integer.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("number_of_premises",Integer.parseInt(numberOfPremises))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "requests_with_premises_with_garbage_less_than_specified(" +duplicates + "," + numberOfPremises  +")");
        return spQuery.getResultList();
    }

    public List<Object> query11(String numberOfPremises, String duplicates)  {

        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("requests_with_premises_with_rats_less_than_specified")
                .registerStoredProcedureParameter(
                        "number_of_premises",
                        Integer.class ,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        "duplicates",
                        Boolean.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("number_of_premises",Integer.parseInt(numberOfPremises))
                .setParameter("duplicates",Boolean.parseBoolean(duplicates));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "requests_with_premises_with_rats_less_than_specified(" +duplicates + "," + numberOfPremises+")");
        return spQuery.getResultList();
    }


    public List<Integer> query12(String date) throws ParseException {


        StoredProcedureQuery spQuery = getSession().createStoredProcedureCall("police_districts_handling_potholes_and_rodent_baiting_in_day")
                .registerStoredProcedureParameter(
                        "date",
                        Date.class ,
                        ParameterMode.IN
                );

        spQuery.setParameter("date",parseDate(date));

        loggingService.addToHistory(SecurityContextHolder.getContext().getAuthentication(),
                "police_districts_handling_potholes_and_rodent_baiting_in_day(" +date+")");
        return spQuery.getResultList();
    }


    private Date parseDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = format.parse(date);
        return  new java.sql.Date(parsed.getTime());
    }


    public Request update(Request request) {
        getSession().merge(request);
        return request;
    }

    public HashMap<String, Object> getByProperty(String key, String value, String from, String quantity, String table) {

        HashMap<String,Object> rs = new HashMap<>();

        rs.put("resultSet", getSession().createCriteria(Request.class)
                .createAlias(table, table)
                .add(Restrictions.eq(table + "." + key, value))
                .setFirstResult(Integer.parseInt(from))
                .setMaxResults(Integer.parseInt(quantity))
                .list());
        rs.put("total",(Number) getSession().createCriteria(Request.class)
                .createAlias(table, table)
                .add(Restrictions.eq(table + "." + key, value))
                .setProjection(Projections.rowCount()).uniqueResult());
        return rs;
    }

    public HashMap<String, Object> getByProperty(String key, Integer value, String from, String quantity, String table) {
        HashMap<String,Object> rs = new HashMap<>();

        rs.put("resultSet", getSession().createCriteria(Request.class)
                .createAlias(table, table)
                .add(Restrictions.eq(table + "." + key, value))
                .setFirstResult(Integer.parseInt(from))
                .setMaxResults(Integer.parseInt(quantity))
                .list());
        rs.put("total",(Number) getSession().createCriteria(Request.class)
                .createAlias(table, table)
                .add(Restrictions.eq(table + "." + key, value))
                .setProjection(Projections.rowCount()).uniqueResult());
        return rs;
    }

    public void add(Request request) {
        if(request.getAbandonedVehicleDetails()!=null)
            request.getAbandonedVehicleDetails().setRequest(request);
        if(request.getGarbageCartsDelivered()!=null)
            request.getGarbageCartsDelivered().setRequest(request);
        if(request.getGraffitiDetails()!=null)
            request.getGraffitiDetails().setRequest(request);
        if(request.getLocationOfTreeInRequest()!=null)
            request.getLocationOfTreeInRequest().setRequest(request);
        if(request.getPotholesFilled()!=null)
            request.getPotholesFilled().setRequest(request);
        if(request.getRecentActionsOnRequest()!=null)
            request.getRecentActionsOnRequest().setRequest(request);
        if(request.getRodentBaitingDetails()!=null)
            request.getRodentBaitingDetails().setRequest(request);
        if(request.getSanitationViolationsInRequest()!=null)
            request.getSanitationViolationsInRequest().setRequest(request);
        if(request.getSsa()!=null)
            request.getSsa().setRequest(request);
        persist(request);
    }
}
