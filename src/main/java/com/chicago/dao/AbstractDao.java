package com.chicago.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

@Repository
@Transactional
public abstract class AbstractDao<PK extends Serializable, T> {

    @PersistenceContext
    EntityManager entityManger;


    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public AbstractDao(){
        this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @SuppressWarnings("unchecked")
    public T getByKey(PK key)  {
        return (T) entityManger.find(persistentClass, key);
    }

    public void persist(T entity) {
        entityManger.persist(entity);
    }

    public void delete(T entity) {
        entityManger.remove(entity);
    }

    protected CriteriaBuilder createEntityCriteria(){
        return entityManger.getCriteriaBuilder();
    }

    protected Session getSession(){
        return entityManger.unwrap(Session.class);
    }



}
