package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sanitation_violations_in_request", schema = "public", catalog = "chicago")
public class SanitationViolationsInRequest {
    private Integer requestId;
    private String violation;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "violation", nullable = true, length = 255)
    public String getViolation() {
        return violation;
    }

    public void setViolation(String violation) {
        this.violation = violation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SanitationViolationsInRequest that = (SanitationViolationsInRequest) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(violation, that.violation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, violation);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
