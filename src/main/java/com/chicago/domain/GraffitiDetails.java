package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "graffiti_details", schema = "public", catalog = "chicago")
public class GraffitiDetails {
    private Integer requestId;
    private String structureType;
    private String surfaceType;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "structure_type", nullable = true, length = 255)
    public String getStructureType() {
        return structureType;
    }

    public void setStructureType(String structureType) {
        this.structureType = structureType;
    }

    @Basic
    @Column(name = "surface_type", nullable = true, length = 255)
    public String getSurfaceType() {
        return surfaceType;
    }

    public void setSurfaceType(String surfaceType) {
        this.surfaceType = surfaceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraffitiDetails that = (GraffitiDetails) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(structureType, that.structureType) &&
                Objects.equals(surfaceType, that.surfaceType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, structureType, surfaceType);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
