package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class ActionsPerTypePK implements Serializable {
    private String requestType;
    private String action;

    @Column(name = "request_type", nullable = false, length = 255)
    @Id
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Column(name = "action", nullable = false, length = 255)
    @Id
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionsPerTypePK that = (ActionsPerTypePK) o;
        return Objects.equals(requestType, that.requestType) &&
                Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestType, action);
    }
}
