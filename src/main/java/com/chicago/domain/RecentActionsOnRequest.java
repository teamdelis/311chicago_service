package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "recent_actions_on_request", schema = "public", catalog = "chicago")
public class RecentActionsOnRequest {
    private Integer requestId;
    private String currentActivity;
    private String mostRecentAction;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "current_activity", nullable = true, length = 255)
    public String getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(String currentActivity) {
        this.currentActivity = currentActivity;
    }

    @Basic
    @Column(name = "most_recent_action", nullable = true, length = 255)
    public String getMostRecentAction() {
        return mostRecentAction;
    }

    public void setMostRecentAction(String mostRecentAction) {
        this.mostRecentAction = mostRecentAction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecentActionsOnRequest that = (RecentActionsOnRequest) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(currentActivity, that.currentActivity) &&
                Objects.equals(mostRecentAction, that.mostRecentAction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, currentActivity, mostRecentAction);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
