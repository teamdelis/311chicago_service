package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "street_names", schema = "public", catalog = "chicago")
public class StreetNames {
    private String streetName;
    private Collection<Request> requests;

    @Id
    @Column(name = "street_name", nullable = false, length = 255)
    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreetNames that = (StreetNames) o;
        return Objects.equals(streetName, that.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(streetName);
    }

    @OneToMany(mappedBy = "streetName")
    @JsonBackReference
    public Collection<Request> getRequests() {
        return requests;
    }

    public void setRequests(Collection<Request> requests) {
        this.requests = requests;
    }
}
