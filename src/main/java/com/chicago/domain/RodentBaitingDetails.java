package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "rodent_baiting_details", schema = "public", catalog = "chicago")
public class RodentBaitingDetails {
    private Integer requestId;
    private Integer numberOfPremisesBaited;
    private Integer numberOfPremisesWithGarbage;
    private Integer numberOfPremisesWithRats;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "number_of_premises_baited", nullable = true)
    public Integer getNumberOfPremisesBaited() {
        return numberOfPremisesBaited;
    }

    public void setNumberOfPremisesBaited(Integer numberOfPremisesBaited) {
        this.numberOfPremisesBaited = numberOfPremisesBaited;
    }

    @Basic
    @Column(name = "number_of_premises_with_garbage", nullable = true)
    public Integer getNumberOfPremisesWithGarbage() {
        return numberOfPremisesWithGarbage;
    }

    public void setNumberOfPremisesWithGarbage(Integer numberOfPremisesWithGarbage) {
        this.numberOfPremisesWithGarbage = numberOfPremisesWithGarbage;
    }

    @Basic
    @Column(name = "number_of_premises_with_rats", nullable = true)
    public Integer getNumberOfPremisesWithRats() {
        return numberOfPremisesWithRats;
    }

    public void setNumberOfPremisesWithRats(Integer numberOfPremisesWithRats) {
        this.numberOfPremisesWithRats = numberOfPremisesWithRats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RodentBaitingDetails that = (RodentBaitingDetails) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(numberOfPremisesBaited, that.numberOfPremisesBaited) &&
                Objects.equals(numberOfPremisesWithGarbage, that.numberOfPremisesWithGarbage) &&
                Objects.equals(numberOfPremisesWithRats, that.numberOfPremisesWithRats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, numberOfPremisesBaited, numberOfPremisesWithGarbage, numberOfPremisesWithRats);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
