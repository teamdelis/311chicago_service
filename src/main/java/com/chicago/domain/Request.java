package com.chicago.domain;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Request {
    private Integer id;
    private Integer communityArea;
    private Date completionDate;
    private Date creationDate;
    private Double latitude;
    private Double longitude;
    private Integer policeDistrict;
    private String serviceRequestNumber;
    private String status;
    private String streetNumber;
    private Integer ward;
    private Double xCoordinate;
    private Double yCoordinate;
    private AbandonedVehicleDetails abandonedVehicleDetails;
    private GarbageCartsDelivered garbageCartsDelivered;
    private GraffitiDetails graffitiDetails;
    private LocationOfTreeInRequest locationOfTreeInRequest;
    private PotholesFilled potholesFilled;
    private RecentActionsOnRequest recentActionsOnRequest;
    private RequestType requestType;
    private StreetNames streetName;
    private ZipCodes zipCode;
    private RodentBaitingDetails rodentBaitingDetails;
    private SanitationViolationsInRequest sanitationViolationsInRequest;
    private SsaInRequest ssa;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "request_id_generator")
    @SequenceGenerator(name="request_id_generator", sequenceName = "request_id_sequence",initialValue = 4145289, allocationSize = 1)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "community_area", nullable = true)
    public Integer getCommunityArea() {
        return communityArea;
    }

    public void setCommunityArea(Integer communityArea) {
        this.communityArea = communityArea;
    }

    @Basic
    @Column(name = "completion_date", nullable = true)
    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    @Basic
    @Column(name = "creation_date", nullable = true)
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Basic
    @Column(name = "latitude", nullable = true, precision = 0)
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude", nullable = true, precision = 0)
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "police_district", nullable = true)
    public Integer getPoliceDistrict() {
        return policeDistrict;
    }

    public void setPoliceDistrict(Integer policeDistrict) {
        this.policeDistrict = policeDistrict;
    }

    @Basic
    @Column(name = "service_request_number", nullable = true, length = 255)
    public String getServiceRequestNumber() {
        return serviceRequestNumber;
    }

    public void setServiceRequestNumber(String serviceRequestNumber) {
        this.serviceRequestNumber = serviceRequestNumber;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 255)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "street_number", nullable = true, length = 255)
    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Basic
    @Column(name = "ward", nullable = true)
    public Integer getWard() {
        return ward;
    }

    public void setWard(Integer ward) {
        this.ward = ward;
    }

    @Basic
    @Column(name = "x_coordinate", nullable = true, precision = 0)
    public Double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    @Basic
    @Column(name = "y_coordinate", nullable = true, precision = 0)
    public Double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(id, request.id) &&
                Objects.equals(communityArea, request.communityArea) &&
                Objects.equals(completionDate, request.completionDate) &&
                Objects.equals(creationDate, request.creationDate) &&
                Objects.equals(latitude, request.latitude) &&
                Objects.equals(longitude, request.longitude) &&
                Objects.equals(policeDistrict, request.policeDistrict) &&
                Objects.equals(serviceRequestNumber, request.serviceRequestNumber) &&
                Objects.equals(status, request.status) &&
                Objects.equals(streetNumber, request.streetNumber) &&
                Objects.equals(ward, request.ward) &&
                Objects.equals(xCoordinate, request.xCoordinate) &&
                Objects.equals(yCoordinate, request.yCoordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, communityArea, completionDate, creationDate, latitude, longitude, policeDistrict, serviceRequestNumber, status, streetNumber, ward, xCoordinate, yCoordinate);
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public AbandonedVehicleDetails getAbandonedVehicleDetails() {
        return abandonedVehicleDetails;
    }

    public void setAbandonedVehicleDetails(AbandonedVehicleDetails abandonedVehicleDetails) {
        this.abandonedVehicleDetails = abandonedVehicleDetails;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public GarbageCartsDelivered getGarbageCartsDelivered() {
        return garbageCartsDelivered;
    }

    public void setGarbageCartsDelivered(GarbageCartsDelivered garbageCartsDelivered) {
        this.garbageCartsDelivered = garbageCartsDelivered;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public GraffitiDetails getGraffitiDetails() {
        return graffitiDetails;
    }

    public void setGraffitiDetails(GraffitiDetails graffitiDetails) {
        this.graffitiDetails = graffitiDetails;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public LocationOfTreeInRequest getLocationOfTreeInRequest() {
        return locationOfTreeInRequest;
    }

    public void setLocationOfTreeInRequest(LocationOfTreeInRequest locationOfTreeInRequest) {
        this.locationOfTreeInRequest = locationOfTreeInRequest;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public PotholesFilled getPotholesFilled() {
        return potholesFilled;
    }

    public void setPotholesFilled(PotholesFilled potholesFilled) {
        this.potholesFilled = potholesFilled;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public RecentActionsOnRequest getRecentActionsOnRequest() {
        return recentActionsOnRequest;
    }

    public void setRecentActionsOnRequest(RecentActionsOnRequest recentActionsOnRequest) {
        this.recentActionsOnRequest = recentActionsOnRequest;
    }

    @ManyToOne
    @JoinColumn(name = "request_type", referencedColumnName = "type")
    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    @ManyToOne
    @JoinColumn(name = "street_name", referencedColumnName = "street_name")
    public StreetNames getStreetName() {
        return streetName;
    }

    public void setStreetName(StreetNames streetName) {
        this.streetName = streetName;
    }

    @ManyToOne
    @JoinColumn(name = "zip_code", referencedColumnName = "zip_code")
    public ZipCodes getZipCode() {
        return zipCode;
    }

    public void setZipCode(ZipCodes zipCode) {
        this.zipCode = zipCode;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public RodentBaitingDetails getRodentBaitingDetails() {
        return rodentBaitingDetails;
    }

    public void setRodentBaitingDetails(RodentBaitingDetails rodentBaitingDetails) {
        this.rodentBaitingDetails = rodentBaitingDetails;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public SanitationViolationsInRequest getSanitationViolationsInRequest() {
        return sanitationViolationsInRequest;
    }

    public void setSanitationViolationsInRequest(SanitationViolationsInRequest sanitationViolationsInRequest) {
        this.sanitationViolationsInRequest = sanitationViolationsInRequest;
    }

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    public SsaInRequest getSsa() {
        return ssa;
    }

    public void setSsa(SsaInRequest ssa) {
        this.ssa = ssa;
    }
}
