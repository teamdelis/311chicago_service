package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class ActivitiesPerTypePK implements Serializable {
    private String requestType;
    private String activity;

    @Column(name = "request_type", nullable = false, length = 255)
    @Id
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Column(name = "activity", nullable = false, length = 255)
    @Id
    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivitiesPerTypePK that = (ActivitiesPerTypePK) o;
        return Objects.equals(requestType, that.requestType) &&
                Objects.equals(activity, that.activity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestType, activity);
    }
}
