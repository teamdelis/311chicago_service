package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "potholes_filled", schema = "public", catalog = "chicago")
public class PotholesFilled {
    private Integer requestId;
    private Double numberOfFilledPotholes;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "number_of_filled_potholes", nullable = true, precision = 0)
    public Double getNumberOfFilledPotholes() {
        return numberOfFilledPotholes;
    }

    public void setNumberOfFilledPotholes(Double numberOfFilledPotholes) {
        this.numberOfFilledPotholes = numberOfFilledPotholes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PotholesFilled that = (PotholesFilled) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(numberOfFilledPotholes, that.numberOfFilledPotholes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, numberOfFilledPotholes);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
