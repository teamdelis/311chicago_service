package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "structures_with_graffiti", schema = "public", catalog = "chicago")
public class StructuresWithGraffiti {
    private String structure;

    @Id
    @Column(name = "structure", nullable = false, length = 255)
    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StructuresWithGraffiti that = (StructuresWithGraffiti) o;
        return Objects.equals(structure, that.structure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(structure);
    }
}
