package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "garbage_carts_delivered", schema = "public", catalog = "chicago")
public class GarbageCartsDelivered {
    private Integer requestId;
    private Double numberOfCarts;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "number_of_carts", nullable = true, precision = 0)
    public Double getNumberOfCarts() {
        return numberOfCarts;
    }

    public void setNumberOfCarts(Double numberOfCarts) {
        this.numberOfCarts = numberOfCarts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GarbageCartsDelivered that = (GarbageCartsDelivered) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(numberOfCarts, that.numberOfCarts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, numberOfCarts);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
