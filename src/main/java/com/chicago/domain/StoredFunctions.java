package com.chicago.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "stored_functions", schema = "public", catalog = "chicago")
public class StoredFunctions {
    private String c1;

    @Basic
    @Column(name = "c1", nullable = true, length = -1)
    public String getC1() {
        return c1;
    }

    public void setC1(String c1) {
        this.c1 = c1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoredFunctions that = (StoredFunctions) o;
        return Objects.equals(c1, that.c1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(c1);
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
