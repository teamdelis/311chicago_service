package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "sanitation_code_violations", schema = "public", catalog = "chicago")
public class SanitationCodeViolations {
    private String codeViolation;

    @Id
    @Column(name = "code_violation", nullable = false, length = 255)
    public String getCodeViolation() {
        return codeViolation;
    }

    public void setCodeViolation(String codeViolation) {
        this.codeViolation = codeViolation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SanitationCodeViolations that = (SanitationCodeViolations) o;
        return Objects.equals(codeViolation, that.codeViolation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codeViolation);
    }
}
