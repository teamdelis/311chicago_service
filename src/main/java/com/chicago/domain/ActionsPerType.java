package com.chicago.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "actions_per_type", schema = "public", catalog = "chicago")
@IdClass(ActionsPerTypePK.class)
public class ActionsPerType {
    private String requestType;
    private String action;

    @Id
    @Column(name = "action", nullable = false, length = 255)
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Id
    @Column(name = "request_type", nullable = false, length = 255)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionsPerType that = (ActionsPerType) o;
        return Objects.equals(requestType, that.requestType) &&
                Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestType, action);
    }
}
