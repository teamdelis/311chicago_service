package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "abandoned_vehicle_details", schema = "public", catalog = "chicago")
public class AbandonedVehicleDetails {

    private Integer requestId;
    private String licensePlate;
    private Double numberOfDatsParked;
    private String vehicleColor;
    private String vehicleMakeModel;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "license_plate", nullable = true, length = 255)
    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Basic
    @Column(name = "number_of_dats_parked", nullable = true, precision = 0)
    public Double getNumberOfDatsParked() {
        return numberOfDatsParked;
    }

    public void setNumberOfDatsParked(Double numberOfDatsParked) {
        this.numberOfDatsParked = numberOfDatsParked;
    }

    @Basic
    @Column(name = "vehicle_color", nullable = true, length = 255)
    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    @Basic
    @Column(name = "vehicle_make_model", nullable = true, length = 255)
    public String getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    public void setVehicleMakeModel(String vehicleMakeModel) {
        this.vehicleMakeModel = vehicleMakeModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbandonedVehicleDetails that = (AbandonedVehicleDetails) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(licensePlate, that.licensePlate) &&
                Objects.equals(numberOfDatsParked, that.numberOfDatsParked) &&
                Objects.equals(vehicleColor, that.vehicleColor) &&
                Objects.equals(vehicleMakeModel, that.vehicleMakeModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, licensePlate, numberOfDatsParked, vehicleColor, vehicleMakeModel);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
