package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "location_of_tree_in_request", schema = "public", catalog = "chicago")
public class LocationOfTreeInRequest {
    private Integer requestId;
    private String treeLocation;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "tree_location", nullable = true, length = 255)
    public String getTreeLocation() {
        return treeLocation;
    }

    public void setTreeLocation(String treeLocation) {
        this.treeLocation = treeLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationOfTreeInRequest that = (LocationOfTreeInRequest) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(treeLocation, that.treeLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, treeLocation);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
