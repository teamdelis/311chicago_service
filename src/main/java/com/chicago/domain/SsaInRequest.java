package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ssa_in_request", schema = "public", catalog = "chicago")
public class SsaInRequest {
    private Integer requestId;
    private Integer ssa;
    private Request request;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "ssa", nullable = true)
    public Integer getSsa() {
        return ssa;
    }

    public void setSsa(Integer ssa) {
        this.ssa = ssa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SsaInRequest that = (SsaInRequest) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(ssa, that.ssa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, ssa);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    @JsonBackReference
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
