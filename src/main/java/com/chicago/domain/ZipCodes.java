package com.chicago.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "zip_codes", schema = "public", catalog = "chicago")
public class ZipCodes {
    private Integer zipCode;
    private Collection<Request> requests;

    @Id
    @Column(name = "zip_code", nullable = false)
    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ZipCodes zipCodes = (ZipCodes) o;
        return Objects.equals(zipCode, zipCodes.zipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zipCode);
    }

    @OneToMany(mappedBy = "zipCode")
    @JsonBackReference
    public Collection<Request> getRequests() {
        return requests;
    }

    public void setRequests(Collection<Request> requests) {
        this.requests = requests;
    }
}
