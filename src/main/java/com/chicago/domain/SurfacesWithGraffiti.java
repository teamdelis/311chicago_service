package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "surfaces_with_graffiti", schema = "public", catalog = "chicago")
public class SurfacesWithGraffiti {
    private String surface;

    @Id
    @Column(name = "surface", nullable = false, length = 255)
    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SurfacesWithGraffiti that = (SurfacesWithGraffiti) o;
        return Objects.equals(surface, that.surface);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surface);
    }
}
