package com.chicago.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "vehicle_make_model", schema = "public", catalog = "chicago")
public class VehicleMakeModel {
    private String makeModel;

    @Id
    @Column(name = "make_model", nullable = false, length = 255)
    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleMakeModel that = (VehicleMakeModel) o;
        return Objects.equals(makeModel, that.makeModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(makeModel);
    }
}
