package com.chicago.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "activities_per_type", schema = "public", catalog = "chicago")
@IdClass(ActivitiesPerTypePK.class)
public class ActivitiesPerType {
    private String requestType;
    private String activity;

    @Id
    @Column(name = "activity", nullable = false, length = 255)
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Id
    @Column(name = "request_type", nullable = false, length = 255)
    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivitiesPerType that = (ActivitiesPerType) o;
        return Objects.equals(requestType, that.requestType) &&
                Objects.equals(activity, that.activity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestType, activity);
    }
}
