package com.chicago.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {
	
	private static final long serialVersionUID = -713936443651224272L;

	public ResourceNotFoundException() {
        super("Resource Not Found");
    }

    public ResourceNotFoundException(String id, String resourceType) {
        super(resourceType + " with id " + id + " was not found");
    }
}
