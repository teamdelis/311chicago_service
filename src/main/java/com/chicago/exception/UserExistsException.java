package com.chicago.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserExistsException extends Exception {

    public UserExistsException() {
        super("User already exists!");
    }

    public UserExistsException(String email) {
        super("User with username: " + email + " already exists!");
    }

}
