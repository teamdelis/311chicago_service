package com.chicago.service;

import com.chicago.dao.UserDao;
import com.chicago.domain.Logs;
import com.chicago.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

@Service("logggingService")
@Transactional
public class LoggingService {

    @Autowired
    UserDao userDao;


    public void addToHistory(Authentication authentication, String query) {
        String username = ((UserDetails)authentication.getPrincipal()).getUsername();

        User user = userDao.getById(username);
        Logs logs = new Logs();
        logs.setEvent(query);
        logs.setTimestamp(new Timestamp(System.currentTimeMillis()));

        user.getLogs().add(logs);
        userDao.update(user);
    }
}
