package com.chicago.service;


import com.chicago.domain.User;

import java.util.List;

public interface UserServiceApi {

    public User login(String username, String password);

    boolean exists(User user);

    void register(User user);

    List getAllUsernames();
}

