package com.chicago.service;

import com.chicago.dao.RequestDao;
import com.chicago.domain.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

@Service("requestService")
@Transactional
public class RequestService implements RequestServiceApi {

    @Autowired
    RequestDao requestDao;

    @Override
    public Request getRequestById(Integer id) {
        return requestDao.getById(id);
    }

    @Override
    public List<Object> query1(String duplicates, String from, String to) throws ParseException {
        return requestDao.query1(duplicates,from,to);
    }

    @Override
    public List<Object> query2(String type, String duplicates, String from, String to) throws ParseException {
        return requestDao.query2(type,duplicates,from,to);
    }

    @Override
    public List<Object> query3(String duplicates, String date) throws ParseException {
        return requestDao.query3(duplicates,date);
    }

    @Override
    public List<Object> query4(String duplicates, String from, String to) throws ParseException {
        return requestDao.query4(duplicates,from,to);
    }

    @Override
    public List<Object> query5(String x1, String y1, String x2, String y2, String duplicates, String date) throws ParseException {
        return requestDao.query5(x1, y1, x2, y2, duplicates, date);
    }

    @Override
    public List<Object> query6(String duplicates, String from, String to) throws ParseException {
        return requestDao.query6(duplicates,from,to);
    }

    @Override
    public List<Object> query7(String duplicates) throws ParseException {
        return requestDao.query7(duplicates);
    }

    @Override
    public List<Object> query8(String duplicates) throws ParseException {
        return requestDao.query8(duplicates);
    }

    @Override
    public List<Object> query9(String numberOfPremises, String duplicates)  {
        return requestDao.query9(numberOfPremises,duplicates);
    }

    @Override
    public List<Object> query10(String numberOfPremises, String duplicates)  {
        return requestDao.query10(numberOfPremises,duplicates);
    }

    @Override
    public List<Object> query11(String numberOfPremises, String duplicates)  {
        return requestDao.query11(numberOfPremises,duplicates);
    }

    @Override
    public List<Integer> query12(String date) throws ParseException {
        return requestDao.query12(date);
    }

    @Override
    public Request update(Request request) {
        return requestDao.update(request);
    }

    @Override
    public Request add(Request request) {
        requestDao.add(request);
        return request;
    }

    @Override
    public HashMap getByZipCode(Integer code, String from, String quantity) {
        return requestDao.getByProperty("zipCode",code,from,quantity,"zipCode");
    }

    @Override
    public HashMap getByStreetName(String name, String from, String quantity) {
        return requestDao.getByProperty("streetName",name,from,quantity,"streetName");
    }
}
