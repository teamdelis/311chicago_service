package com.chicago.service;

import com.chicago.dao.UserDao;
import com.chicago.domain.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional
public class UserService implements UserServiceApi {

    @Autowired
    UserDao userDao;

    @Override
    public User login(String username, String password) {
        return userDao.login(username,password);
    }

    @Override
    public boolean exists(User user) {
        return userDao.exists(user);
    }

    @Override
    public void register(User user) {
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        userDao.persist(user);
    }

    @Override
    public List getAllUsernames() {
        return userDao.getAllUsernames();
    }
}
