package com.chicago.service;

import com.chicago.domain.Request;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public interface RequestServiceApi {

    public Request getRequestById(Integer id);

    List<Object> query1(String duplicates, String from, String to) throws ParseException;

    List<Object> query2(String type, String duplicates, String from, String to) throws ParseException;

    List<Object> query3(String duplicates, String date) throws ParseException;

    List<Object> query4(String duplicates, String from, String to) throws ParseException;

    List<Object> query5(String x1, String y1, String x2, String y2, String duplicates, String date) throws ParseException;

    List<Object> query6(String duplicates, String from, String to) throws ParseException;

    List<Object> query7(String duplicates) throws ParseException;

    List<Object> query8(String duplicates) throws ParseException;

    List<Object> query9(String numberOfPremises, String duplicates);

    List<Object> query10(String numberOfPremises, String duplicates);

    List<Object> query11(String numberOfPremises, String duplicates);

    List<Integer> query12(String date) throws ParseException;

    Request update(Request request);

    Request add(Request request);

    HashMap getByZipCode(Integer code, String from, String quantity);

    HashMap getByStreetName(String name, String from, String quantity);
}
