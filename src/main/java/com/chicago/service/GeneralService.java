package com.chicago.service;

import com.chicago.dao.GeneralDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("generalService")
@Transactional
public class GeneralService implements GeneralServiceApi {

    @Autowired
    GeneralDao generalDao;

    @Override
    public List getAll(Class c) {
        return generalDao.getAll(c);
    }

    @Override
    public List getByType(Class c, String type,String field) {
        return generalDao.getByType(c,type,field);
    }
}
