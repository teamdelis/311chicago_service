package com.chicago.controllers;

import com.chicago.domain.Request;
import com.chicago.exception.ResourceNotFoundException;
import com.chicago.service.RequestServiceApi;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/request")
@Api(description = "Request API",  tags = {"request"})
public class RequestController {


    @Autowired
    RequestServiceApi requestService;


    @RequestMapping(value = "/getByID/{id}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Request getRequestByID(@PathVariable("id")  Integer id) throws ResourceNotFoundException {
        Request request = requestService.getRequestById(id);
        if(request == null)
            throw new ResourceNotFoundException();
        return request;
    }

    @RequestMapping(value = "/getByStreetName" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    HashMap getByStreetName(@RequestParam("name")  String name,
                                  @RequestParam("from")  String from,
                                  @RequestParam("quantity")  String quantity){
       return requestService.getByStreetName(name,from,quantity);
    }

    @RequestMapping(value = "/getByZipCode" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    HashMap getByZipCode(@RequestParam("code")  Integer code,
                         @RequestParam("from")  String from,
                         @RequestParam("quantity")  String quantity){
        return requestService.getByZipCode(code,from,quantity);
    }


    @RequestMapping(value = "/update" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Request update(@RequestBody Request request) throws ResourceNotFoundException {
       return requestService.update(request);
    }

    @RequestMapping(value = "/add" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Request add(@RequestBody Request request) throws ResourceNotFoundException {
        return requestService.add(request);
    }

    @RequestMapping(value = "/query1" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query1(@RequestParam("duplicates") String duplicates,
                                             @RequestParam("from") String from,
                                             @RequestParam("to") String to) throws  ParseException {
        return requestService.query1(duplicates,from,to);
    }

    @RequestMapping(value = "/query2" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query2(@RequestParam("type") String type,
                                    @RequestParam("duplicates") String duplicates,
                                     @RequestParam("from") String from,
                                     @RequestParam("to") String to) throws  ParseException {
        return requestService.query2(type,duplicates,from,to);
    }

    @RequestMapping(value = "/query3" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query3(@RequestParam("duplicates") String duplicates,
                                                     @RequestParam("date") String date) throws  ParseException {
        return requestService.query3(duplicates,date);
    }


    @RequestMapping(value = "/query4" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query4(@RequestParam("duplicates") String duplicates,
                                     @RequestParam("from") String from,
                                     @RequestParam("to") String to) throws  ParseException {
        return requestService.query4(duplicates,from,to);
    }

    @RequestMapping(value = "/query5" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query5(@RequestParam("x1") String x1,
                                            @RequestParam("y2") String y1,
                                            @RequestParam("x2") String x2,
                                            @RequestParam("y2") String y2,
                                            @RequestParam("duplicates") String duplicates,
                                          @RequestParam("date") String date) throws  ParseException {
        return requestService.query5(x1, y1, x2, y2, duplicates, date);
    }

    @RequestMapping(value = "/query6" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query6(@RequestParam("duplicates") String duplicates,
                                          @RequestParam("from") String from,
                                          @RequestParam("to") String to) throws  ParseException {
        return requestService.query6(duplicates,from,to);
    }


    @RequestMapping(value = "/query7" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query7(@RequestParam("duplicates") String duplicates) throws  ParseException {
        return requestService.query7(duplicates);
    }

    @RequestMapping(value = "/query8" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query8(@RequestParam("duplicates") String duplicates) throws  ParseException {
        return requestService.query8(duplicates);
    }

    @RequestMapping(value = "/query9" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query9(@RequestParam("numberOfPremises") String numberOfPremises,
                                   @RequestParam("duplicates") String duplicates)  {
        return requestService.query9(numberOfPremises,duplicates);
    }

    @RequestMapping(value = "/query10" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query10(@RequestParam("numberOfPremises") String numberOfPremises,
                                                                     @RequestParam("duplicates") String duplicates)  {
        return requestService.query10(numberOfPremises,duplicates);
    }

    @RequestMapping(value = "/query11" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query11(@RequestParam("numberOfPremises") String numberOfPremises,
                                                                      @RequestParam("duplicates") String duplicates)  {
        return requestService.query11(numberOfPremises,duplicates);
    }

    @RequestMapping(value = "/query12" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Integer> query12(@RequestParam("date") String date) throws ParseException {
        return requestService.query12(date);
    }



}
