package com.chicago.controllers;

import com.chicago.domain.User;
import com.chicago.exception.UserExistsException;
import com.chicago.exception.UserNotFoundException;
import com.chicago.service.UserServiceApi;
import com.sun.deploy.net.HttpResponse;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
@Api(description = "User API",  tags = {"user"})
public class UserController {


    @Autowired
    UserServiceApi userService;


    @RequestMapping(value = "/register" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User register(@RequestBody User user) throws UserExistsException {
        if(!userService.exists(user))
            userService.register(user);
        else
            throw new UserExistsException(user.getUsername());
        return user;
    }


    @RequestMapping(value = "/login" , method = RequestMethod.POST)
    public void login(HttpServletRequest request) throws UserNotFoundException {
        String username = request.getHeader("username");
        String password = request.getHeader("password");
        User user =  userService.login(username,password);
        if(user == null)
           throw new UserNotFoundException();
    }

    @RequestMapping(value = "/getAllUsernames" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List getAllUsernames() {
        return userService.getAllUsernames();
    }


    @RequestMapping(value = "/logout" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void logout(HttpSession session, HttpServletResponse response) {
        session.invalidate();
        Cookie cookie = new Cookie("currentUser", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

}
