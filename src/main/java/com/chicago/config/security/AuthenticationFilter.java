package com.chicago.config.security;

import com.chicago.service.UserService;
import com.chicago.service.UserServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AuthenticationFilter extends GenericFilterBean{

    @Autowired
    UserServiceApi userService;

    @Override
    public void doFilter(
            ServletRequest req,
            ServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if(SecurityContextHolder.getContext().getAuthentication() == null){

            final List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
            final UserDetails principal = new org.springframework.security.core.userdetails.User(request.getHeader("username"),
                    request.getHeader("password"),
                    grantedAuths);
            final Authentication auth = new UsernamePasswordAuthenticationToken(principal,  request.getHeader("password"), grantedAuths);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        Cookie sessionCookie = new Cookie("currentUser", request.getHeader("username"));

        int expireSec = 14400;
        sessionCookie.setMaxAge(expireSec);
        sessionCookie.setPath("/");
        response.addCookie(sessionCookie);
        chain.doFilter(req, res);
    }
}